package com.ericbraga.turistar.manager;

import android.app.Activity;
import com.ericbraga.turistar.pojo.CartesianPoint;
import com.ericbraga.turistar.pojo.LocationValues;

public class BusinessManager {

	private ScreenManager screenManager;

	private CartesianPoint currentCartesianPoint;

	public BusinessManager(Activity activity) {
		this.screenManager = new ScreenManager(activity);
	}

	public void setCurrentCartesianPoint(CartesianPoint currentCartesianPoint) {
		this.currentCartesianPoint = currentCartesianPoint;
	}

	public boolean isPointToBeShow(LocationValues candidatePoint) {
		if (this.currentCartesianPoint != null) {
			return this.isPointToBeShow(candidatePoint, 0.0f);
		} else {
			return false;
		}
	}

	public boolean isPointToBeShow(LocationValues candidatePoint, float maximumDistance) {
		if (this.currentCartesianPoint != null) {
			if (maximumDistance == 0.0f) {
				return this.currentCartesianPoint
						.isLocationPointIntoVisionAngle(candidatePoint);

			} else {
				return currentCartesianPoint.distanceTo(candidatePoint) < maximumDistance
						&& this.currentCartesianPoint
								.isLocationPointIntoVisionAngle(candidatePoint);
			}
		} else {
			return false;
		}
	}

	public float distancePointToPrintElement(LocationValues candidatePoint) {
		double angleBetweenMeAndTuristicPoint = this.currentCartesianPoint
				.angleBetweenHeading(candidatePoint);

		float distancePrintPoint = screenManager
				.getDistanceToPrintPoint(angleBetweenMeAndTuristicPoint);

		if (!currentCartesianPoint.isOtherPointInLeft(candidatePoint)) {
			distancePrintPoint = (screenManager.getCurrentScreenSize() / 2)
					- distancePrintPoint;

		} else {
			distancePrintPoint = (screenManager.getCurrentScreenSize() / 2)
					+ distancePrintPoint;
		}

		return distancePrintPoint;
	}
}
