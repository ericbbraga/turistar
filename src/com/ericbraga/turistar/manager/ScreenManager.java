package com.ericbraga.turistar.manager;

import android.app.Activity;

import com.ericbraga.turistar.pojo.CartesianPoint;

public class ScreenManager {
    private Activity mActivity;

    public ScreenManager(Activity activity) {
        super();
        if (activity != null) {
            mActivity = activity;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Simples regra de três para saber a que
     * distância deve imprimir o ponto do centro
     * @return
     */
    public float getDistanceToPrintPoint(double angle) {
        final int halfscreenSize = getCurrentScreenSize() / 2;

        double screenPointMarker = (halfscreenSize * angle) / CartesianPoint.ANGLE_OF_VISION;
        android.util.Log.i("ericbraga", "percentScreen = " + screenPointMarker);
        return (float) screenPointMarker;
    }

    public int getCurrentScreenSize() {
        return mActivity.getWindowManager().getDefaultDisplay().getWidth();
    }



}
