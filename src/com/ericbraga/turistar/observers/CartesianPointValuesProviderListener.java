package com.ericbraga.turistar.observers;

import com.ericbraga.turistar.pojo.CartesianPoint;

public interface CartesianPointValuesProviderListener {
    public abstract void cartesianPointReady();

    public abstract void updateOrientationValue(CartesianPoint cartesianPoint);
}
