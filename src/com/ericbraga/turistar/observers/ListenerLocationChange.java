package com.ericbraga.turistar.observers;

import com.ericbraga.turistar.pojo.LocationValues;

public interface ListenerLocationChange {
    public abstract void updateLocationValues(LocationValues locationValues);
}
