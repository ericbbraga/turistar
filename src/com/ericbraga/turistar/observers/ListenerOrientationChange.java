package com.ericbraga.turistar.observers;

import com.ericbraga.turistar.pojo.SensorOrientationValues;

public interface ListenerOrientationChange {
    public abstract void updateValuesSensor(SensorOrientationValues sensorValues);
}
