package com.ericbraga.turistar.opengl;

import java.io.IOException;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager.LayoutParams;

public class CameraView implements SurfaceHolder.Callback{

    private Camera mCamera;

    private SurfaceView mSurfaceView;

    private SurfaceHolder mSurfaceHolder;

    private Context mContext;

    public CameraView(Context context) {
        super();
        mContext = context;

        mSurfaceView = new SurfaceView(mContext);
        mSurfaceHolder = mSurfaceView.getHolder();

        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mSurfaceHolder.setFormat(PixelFormat.TRANSLUCENT | LayoutParams.FLAG_BLUR_BEHIND);
        mSurfaceHolder.addCallback(this);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPreviewSize(width, height);

        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            // Avisar ao chamador que ocorreu uma exceção
        }

        mCamera.startPreview();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
    }

    public SurfaceView getSurfaceView() {
        return mSurfaceView;
    }

}
