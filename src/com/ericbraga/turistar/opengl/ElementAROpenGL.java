package com.ericbraga.turistar.opengl;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class ElementAROpenGL extends GLSurfaceView {

    private ElementOpenGLRenderer turistAROpenGLRenderer;

    public ElementAROpenGL(Context context) {
        super(context);
        turistAROpenGLRenderer = new ElementOpenGLRenderer();

        /* Configurações necessárias para deixar transparente
         * o surfaceview transparente */
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        getHolder().setFormat(PixelFormat.TRANSPARENT);

        setRenderer(turistAROpenGLRenderer);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        queueEvent(new Runnable() {
            public void run() {
                turistAROpenGLRenderer.addTranslateLatitude(0.5f);
            }
        });
        return super.onTouchEvent(event);
    }

    public void addTranslateLatitude(float translate) {
        turistAROpenGLRenderer.addTranslateLatitude(translate);
    }

}
