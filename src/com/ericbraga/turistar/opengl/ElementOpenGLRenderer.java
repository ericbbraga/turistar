package com.ericbraga.turistar.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;

public class ElementOpenGLRenderer implements GLSurfaceView.Renderer {

    private static final int SHORT_BY_BYTES = 2;

    private static final int INITIAL_PLACE = 0;

    private static final int FLOAT_BY_BYTES = 4;

    private static float TRANSLATE_BY_METERS = 0;

    private float mTranslateLongitude;

    public ElementOpenGLRenderer() {
        mTranslateLongitude = 0.0f;
    }

    public void addTranslateLatitude(float translate) {
        android.util.Log.i("ericbraga", "value = " + (mTranslateLongitude + translate));
        mTranslateLongitude = 0 + translate;
    }

    private float vertices[] = {
            0.0f, 0.0f, 0.0f,
            -0.3f, -0.8f, 0.0f,
            0.0f, -0.6f, 0.0f,
            0.3f, -0.8f, 0.0f,
    };

    private short[] indices = { 0, 1, 2, 0, 2, 3 };

    private FloatBuffer vertexBuffer;

    private ShortBuffer indexBuffer;

    public void onDrawFrame(GL10 gl) {
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
        gl.glTranslatef(0, 0, -4);

        gl.glFrontFace(GL10.GL_CCW);
        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glCullFace(GL10.GL_BACK);

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        //gl.glDrawArrays(GL10.GL_TRIANGLES, 0, 3);
        gl.glColor4f(1.0f, 0.0f, 0.0f, 0.3f);
        gl.glDrawElements(GL10.GL_TRIANGLES, indices.length, GL10.GL_UNSIGNED_SHORT, indexBuffer);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisable(GL10.GL_CULL_FACE);
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();

        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f,
                100.0f);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * FLOAT_BY_BYTES);
        vbb.order(ByteOrder.nativeOrder());
        vertexBuffer = vbb.asFloatBuffer();

        vertexBuffer.put(vertices);
        vertexBuffer.position(INITIAL_PLACE);

        ByteBuffer ibb = ByteBuffer.allocateDirect(vertices.length * SHORT_BY_BYTES);
        ibb.order(ByteOrder.nativeOrder());
        indexBuffer = ibb.asShortBuffer();
        indexBuffer.put(indices);
        indexBuffer.position(INITIAL_PLACE);
    }
}
