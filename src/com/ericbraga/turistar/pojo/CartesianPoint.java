package com.ericbraga.turistar.pojo;

/**
 * @author ericbraga
 * Classe que representa um ponto no sistema
 * geoposicional, tendo a sua atual localização
 * juntamente com a direção do dispositivo
 */
public class CartesianPoint {

    public static final int ANGLE_OF_VISION = 60;

    private LocationValues mLocation;

    private SensorOrientationValues sensorValues;

    public CartesianPoint(LocationValues location, SensorOrientationValues sensorValues) {
        super();
        this.mLocation = location;
        this.sensorValues = sensorValues;
    }

    public LocationValues getLocation() {
        return mLocation;
    }

    public SensorOrientationValues getSensorValues() {
        return sensorValues;
    }

    public boolean isLocationPointIntoVisionAngle(LocationValues otherValue) {

        boolean returnValue = false;
        double angleBetweenPointHeading = angleBetweenHeading(otherValue);
        android.util.Log.i("ericbraga3", "angle = " + angleBetweenPointHeading);

        if (ANGLE_OF_VISION >= angleBetweenPointHeading) {
            returnValue = true;
        }
        
        return returnValue;
    }

    /*private LocationValues transposePointToBegining(LocationValues otherValue) {
        double transposeLatitudeValue  = otherValue.getLatitude()  - mLocation.getLatitude();
        double transposeLongitudeValue = otherValue.getLongitude() - mLocation.getLongitude();

        Location transposeLocation = new Location("");
        transposeLocation.setLatitude(transposeLatitudeValue);
        transposeLocation.setLongitude(transposeLongitudeValue);

        return new LocationValues(transposeLocation);
    }*/

    public double angleBetweenHeading(LocationValues otherValue) {
        double angleOfCoordenades = computeAngleOfOrdenades(otherValue);
        double angleReturn = 0;

        /**
         * Se a longitude do ponto testado for de sinal igual ao do ponto atual
         * então o ângulo de retorno deve ser subtraído. Caso contrário deve ser somado
         * */
        if (Math.signum(mLocation.getLongitude()) == Math.signum(otherValue.getLongitude())
        		&& !isOtherPointInLeft(otherValue)) {
            angleReturn = angleOfCoordenades - sensorValues.getHeadingAngle();

        } else {
            angleReturn = angleOfCoordenades + sensorValues.getHeadingAngle();
        }

//        android.util.Log.i("ericbraga", "sensorValue = " + sensorValues.getHeadingAngle());
        return Math.abs(angleReturn);
    }

    /**
     * Para execução deste cálculo é necessário utilizar a propriedade do produto escalar,
     * dado por:
     * Cos O x |A| x |B| = A . B, Logo,
     * Cos O = A  .  B
     *        |A| x |B|
     * 
     * Onde o Vetor A é o vetor definido por (0, 1) e
     * A . B = Xa x Xb + Ya x Yb
     * Como Xa = 0 e Ya = 1, então temos que
     * A . B = Yb. Como |A| é igual a 1
     * então a equação se resume a
     * Cos O = Yb / |B|
     * @param otherValues
     * @return ângulo entre o eixo das ordenadas e o ponto turístico
     */
    private double computeAngleOfOrdenades(LocationValues otherValue) {
        float distanceToPoint = distanceTo(otherValue);
        double angleInRadian = Math.acos(otherValue.getLongitude() / distanceToPoint);

        return Math.toDegrees(angleInRadian);
    }

    public float distanceTo(LocationValues otherValue) {
        return mLocation.distanceTo(otherValue);
    }
    
    /*
     * Para verificar se um ponto está a esquerda ou a direita
     * é necessário a reta formada por dois pontos na reta 
     * do ângulo de visão. Para facilitar os cálculos pegamos
     * os pontos (0,0) e (1, TagAng), onde TangAng pode ser calculado no
     * método calculateTangAngle. Para o cálculo para PointA, PointB e PointC
     * temos que: (Bx - Ax) * (Cy - Ay) - (By - Ay) * (Cx - Ax)
     * Como escolhemos os pontos (0,0) e (1, TangAng) então teremos:
     * (1 - 0) * (Cy - 0) - (TangAng - 0) * (Cx - 0). Simplificando,
     * Cy - TangAng * Cx
     * */
    
    public boolean isOtherPointInLeft(LocationValues otherValue) {
    	double tangAngleBetweenOrdenateAndMe = calculateTangAngle();
    	    	
    	return (otherValue.getLongitude() - 
    				tangAngleBetweenOrdenateAndMe * otherValue.getLatitude() > 0);
    }

	private double calculateTangAngle() {
		final float MAX_ANGLE_PER_COORDENATE = 90;
    	float angleBetweenOrdenateAndMe = MAX_ANGLE_PER_COORDENATE - this.sensorValues.getHeadingAngle();
    	return Math.atan(angleBetweenOrdenateAndMe);
	}
}
