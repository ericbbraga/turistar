package com.ericbraga.turistar.pojo;

import android.location.Location;

/**
 * @author ericbraga
 * Classe que encapsula a latitude e a longitude
 */
public class LocationValues {
    private static final int INDEX_OF_DISTANCE = 0;
    private static final int NUMBER_OF_COORDENATES = 2;
    private Location mLocation;

    public LocationValues(Location location) {
        super();
        if (location != null) {
            mLocation = location;
        } else {
            throw new IllegalArgumentException("Location can not be null");
        }
    }

    public double getLatitude() {
        return mLocation.getLatitude();
    }

    public double getLongitude() {    	
        return mLocation.getLongitude();
    }

    private Location getLocation() {
        return mLocation;
    }

    public void setLatitude(double latitude) {
        mLocation.setLatitude(latitude);
    }

    public void setLongitude(double longitude) {
        mLocation.setLongitude(longitude);
    }

    public boolean isOtherPointInNorthDirection(LocationValues otherValue) {
        return getLatitude() < otherValue.getLatitude();
    }

    public boolean isOtherPointInEastDirection(LocationValues otherValue) {
        return getLongitude() < otherValue.getLongitude();
    }

    public float distanceTo(LocationValues otherValue) {
        return mLocation.distanceTo(otherValue.getLocation());
    }

    public float distanceToLatitude(LocationValues otherValue) {
        float[] result = new float[NUMBER_OF_COORDENATES];
        Location.distanceBetween(mLocation.getLatitude(), 0, otherValue.getLocation().getLatitude(), 0, result);
        return result[INDEX_OF_DISTANCE];
    }

    public float distanceToLongitude(LocationValues otherValue){
        float[] result = new float[NUMBER_OF_COORDENATES];
        Location.distanceBetween(0, mLocation.getLongitude(), 0, otherValue.getLocation().getLongitude(), result);
        return result[INDEX_OF_DISTANCE];
    }

}
