package com.ericbraga.turistar.pojo;

/**
 * @author ericbraga
 * Classe que representa os valores
 * do sensor de orientação
 */
public class SensorOrientationValues {

    private static final int MAX_ROLL_VALUE = 90;

    private static final int MIN_ROLL_VALUE = -90;

    private static final int MAX_PITCH_VALUE = 180;

    private static final int MIN_PITCH_VALUE = -180;

    private static final int MAX_HEADING_VALUE = 180;

    private static final int MIN_HEADING_VALUE = -180;

    /**
     * Os valores do Heading são os que determinam em qual posição
     * o dispositovo está apontando. Os valores variam de 0 até 360.
     * Caso o valor seja 0 / 360 então estaremos apontando para o Norte,
     * 90 para o leste, 180 para o sul e 270 para o oeste.
     * */
    private float headingAngle;

    /**
     * Os valores do Pitch representam quando o dispositivo está sendo rotacionado
     * em torno do maior eixo. Ex.: Caso o dispositivo seja 960 x 640, então estamos
     * rotacionando em volta do 960. Estes valores variam de -180 a 180.
     * Caso o valor do Pitch seja 0 então o dispositivo está apontando para cima,
     * -90 o dispositivo está em pé, 90 o dispositivo está de cabeça para baixo e por último
     * 180/-180 o dispositivo está virado para baixo.
     * */
    private float pitchAngle;

    /**
     * Os valores do Roll são os que representam o rotacionamento no menor eixo.
     * Estes valores variam de -90 a 90. Caso seja 0 significa que o dispositivo
     * está apontando para cima. Caso seja -90 então está com a face para a direita
     * e caso seja 90 está apontando para a esquerda
     * */
    private float rollAngle;

    public SensorOrientationValues() {
        this(0,0,0);
    }

    public SensorOrientationValues(float headingAnle, float pitchAngle, float rollAngle) {
        setHeadingAngle(headingAnle);
        setPitchAngle(pitchAngle);
        setRollAngle(rollAngle);
    }

    public float getHeadingAngle() {
        return headingAngle;
    }

    public float getPitchAngle() {
        return pitchAngle;
    }

    public float getRollAngle() {
        return rollAngle;
    }

    public void setHeadingAngle(float pHeadingAngle) {
        if (pHeadingAngle > MIN_HEADING_VALUE && pHeadingAngle < MAX_HEADING_VALUE) {
            this.headingAngle = pHeadingAngle;
        }
    }

    public void setPitchAngle(float pPitchAngle) {
        if (pPitchAngle > MIN_PITCH_VALUE && pPitchAngle < MAX_PITCH_VALUE) {
            this.pitchAngle = pPitchAngle;
        }
    }

    public void setRollAngle(float pRollAngle) {
        if (pRollAngle > MIN_ROLL_VALUE && pRollAngle < MAX_ROLL_VALUE) {
            this.rollAngle = pRollAngle;
        }
    }
}
