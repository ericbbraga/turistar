package com.ericbraga.turistar.pojo;

import android.location.Location;

public class TuristicLocationValues extends LocationValues {

    private String nameTuristicPoint;
    
    private int locationId;
    
    private static int locationIdCount = 1;

    public TuristicLocationValues(Location location, String name) {
        super(location);
        setNameTuristicPoint(name);
        locationId = locationIdCount++;
    }

    private void setNameTuristicPoint(String n) {
        nameTuristicPoint = n;
    }

    public String getNameOfTuristicPoint() {
        return nameTuristicPoint;
    }
    
    public int getLocationId() {
    	return locationId;
    }
}
