package com.ericbraga.turistar.providers;

import android.content.Context;

import com.ericbraga.turistar.observers.CartesianPointValuesProviderListener;
import com.ericbraga.turistar.observers.ListenerLocationChange;
import com.ericbraga.turistar.observers.ListenerOrientationChange;
import com.ericbraga.turistar.pojo.CartesianPoint;
import com.ericbraga.turistar.pojo.LocationValues;
import com.ericbraga.turistar.pojo.SensorOrientationValues;
import com.ericbraga.turistar.sensors.GpsValuesProvider;
import com.ericbraga.turistar.sensors.SensorOrientationFactory;
import com.ericbraga.turistar.sensors.SensorOrientationManager;

/**
 * Classe responsável por gerenciar os valores do GSP e
 * do sensor de orientação para disponibilizar um objeto
 * Orientation
 * @author ericbraga
 *
 */
public class CartesianPointValuesProvider implements ListenerOrientationChange, ListenerLocationChange {

    private Context mContext;

    private boolean mOrientationValuesProviderActivate;

    private boolean mCartesianPointValuesIsReady;

    private SensorOrientationManager mSensorManager;

    private GpsValuesProvider mGpsValuesProvider;

    private LocationValues mCurrentLocationValues;

    private SensorOrientationValues mCurrentSensorValue;

    private CartesianPointValuesProviderListener mCartesianListener;

    public CartesianPointValuesProvider(Context context, CartesianPointValuesProviderListener cartesianListener) {
        super();
        mContext = context;
        mCartesianListener = cartesianListener;

        mCartesianPointValuesIsReady = false;
        mOrientationValuesProviderActivate = false;
    }

    public void activateOrientationProvider() {
        if (!mOrientationValuesProviderActivate) {
            android.util.Log.i("ericbbraga", "activating");
            mOrientationValuesProviderActivate= true;
            registerSensor();
            registerGpsProvider();
        }
    }

    private void registerSensor() {
        mSensorManager = SensorOrientationFactory.getSensorOrientation(true, mContext, this);
        mSensorManager.registerSensor();
    }

    private void registerGpsProvider() {
        mGpsValuesProvider = new GpsValuesProvider(mContext, this);
    }

    public void deactivateOrientationProvider() {
        if (mOrientationValuesProviderActivate) {
            mOrientationValuesProviderActivate = false;
            unregisterSensor();
            unregisterGpsProvider();
        }
    }

    private void unregisterSensor() {
        mSensorManager.unregisterSensor();
    }

    private void unregisterGpsProvider() {
        mGpsValuesProvider.disabledGpsProvider();
    }

    public void updateValuesSensor(SensorOrientationValues sensorValues) {
        mCurrentSensorValue = sensorValues;
        updateOrientationValues();
    }

    public void updateLocationValues(LocationValues locationValues) {
        mCurrentLocationValues = locationValues;
        updateOrientationValues();
    }

    public void updateOrientationValues() {
        if (mCurrentLocationValues != null && mCurrentSensorValue != null) {

            if (!mCartesianPointValuesIsReady) {
                // Informar ao listenter que deve ser atualizado
                mCartesianPointValuesIsReady = true;
                mCartesianListener.cartesianPointReady();

            } else {
                mCartesianListener.updateOrientationValue(new CartesianPoint(mCurrentLocationValues, mCurrentSensorValue));
            }
        }
    }
}
