package com.ericbraga.turistar.providers;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.ericbraga.turistar.pojo.TuristicLocationValues;
import com.ericbraga.turistar.test.TuristicLocationValuesTest;

public class TuristicLocationProvider {

    private List<TuristicLocationValues> turistLocationList;

    public TuristicLocationProvider(Context context) {
        loadTuristicLocation();
    }

    private void loadTuristicLocation() {
        if (turistLocationList == null) {
            turistLocationList = new ArrayList<TuristicLocationValues>();

        } else {
            turistLocationList.clear();
        }

        // TODO Carregar a lista de Pontos Turísticos
        // Estou carregando uma lista de testes
        turistLocationList = TuristicLocationValuesTest.getTuristLocationListTest();
    }

    public List<TuristicLocationValues> getTuristicLocation() {
        return turistLocationList;
    }
}
