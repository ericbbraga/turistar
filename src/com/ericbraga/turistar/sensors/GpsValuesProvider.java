package com.ericbraga.turistar.sensors;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.ericbraga.turistar.observers.ListenerLocationChange;
import com.ericbraga.turistar.pojo.LocationValues;

/**
 * Classe responsável por gerenciar os valores do GPS
 * e repassar para as interfaces interessadas em modificações
 * destes valores
 * @author ericbraga
 *
 */
public class GpsValuesProvider implements LocationListener {

    private static final int MIN_METERS_TO_UPDATE = 0;

    private static final int MIN_SECONDS_TO_UPDATE = 2500;

    private Context mContext;

    private LocationManager mLocationManger;

    private ListenerLocationChange mListener;

    public GpsValuesProvider(Context context, ListenerLocationChange listener) {
        mContext = context;
        mLocationManger = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        mLocationManger.requestLocationUpdates(bestLocationProviderApproach(), MIN_SECONDS_TO_UPDATE, MIN_METERS_TO_UPDATE, this);
        mListener = listener;
    }

    /* Verifica qual provider é o melhor para ser utilizado pelo gps.
     * Levando em consideração o nível de precisão (Accuracy), custo
     * e nível de bateria a ser utilizado Podemos ter dois providers:
     * LocationManager.GPS_PROVIDER ou LocationManager.NETWORK_PROVIDER*/
    private String bestLocationProviderApproach() {
        Criteria criteriaForProvider = new Criteria();
        criteriaForProvider.setAccuracy(Criteria.ACCURACY_COARSE);
        criteriaForProvider.setAltitudeRequired(false);
        criteriaForProvider.setBearingRequired(false);
        criteriaForProvider.setCostAllowed(false);
        criteriaForProvider.setPowerRequirement(Criteria.POWER_MEDIUM);

        String bestLocation = mLocationManger.getBestProvider(criteriaForProvider, true);

        return bestLocation;
    }

    public void onLocationChanged(Location location) {
        mListener.updateLocationValues(new LocationValues(location));
    }

    public void onProviderDisabled(String provider) { }

    public void onProviderEnabled(String provider) {
        Location location = mLocationManger.getLastKnownLocation(bestLocationProviderApproach());

        if (location != null) {
            mListener.updateLocationValues(new LocationValues(location));
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Location location = mLocationManger.getLastKnownLocation(bestLocationProviderApproach());

        if (location != null) {
            mListener.updateLocationValues(new LocationValues(location));
        }
    }

    public void disabledGpsProvider() {
        mLocationManger.removeUpdates(this);
    }
}