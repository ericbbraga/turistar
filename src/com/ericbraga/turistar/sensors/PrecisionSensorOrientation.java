package com.ericbraga.turistar.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.ericbraga.turistar.observers.ListenerOrientationChange;
import com.ericbraga.turistar.pojo.SensorOrientationValues;

/**
 * @author ericbraga
 * Esta classe faz o cálculo da orientação baseando-se em dois sensores:
 * O de magnetismo e o acelerômetro. Com a utilização deste dois sensores
 * os valores são mais precisos do que utilizando o Sensor de orientação padrão.
 */
public class PrecisionSensorOrientation implements SensorOrientationManager {

    /**
     * O sensor de Magnetísmo e o acelerômetro retornaram os valores entre
     * -180 (ao sul) e 180 (ao Norte) para facilitar o entendimento
     * colocamos os valores entre 0 e 360.
     * */
    private static final int ADD_VALUE_HEADING = 0;

    private static final int MATRIX_VALUES = 9;

    private static final int VALUES_OF_ORIENTATION = 3;

    private boolean mSensorEnabled;

    private SensorManager mSensorManager;

    private Context mContext;

    private ListenerOrientationChange mListener;

    private SensorOrientationValues mSensorValues;

    private float[] mAccelerometerSensorValues;

    private float[] mMagneticSensorValues;

    private final SensorEventListener accelerometerSensor = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mAccelerometerSensorValues = event.values;
                updateSensorValuesWhenNecessary();
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) { /*Não precisei reimplementar*/ }
    };

    private final SensorEventListener magneticSensor = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                mMagneticSensorValues = event.values;
                updateSensorValuesWhenNecessary();
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) { /*Não precisei reimplementar*/ }
    };

    public PrecisionSensorOrientation(Context context, ListenerOrientationChange listener) {
        mContext = context;
        mListener = listener;
        mSensorValues = new SensorOrientationValues();
        mSensorEnabled = false;
    }

    protected void updateSensorValuesWhenNecessary() {
        if (mAccelerometerSensorValues != null && mMagneticSensorValues != null) {
            float[] values = new float[VALUES_OF_ORIENTATION];
            float[] R      = new float[MATRIX_VALUES];
            float[] outR   = new float[MATRIX_VALUES];

            SensorManager.getRotationMatrix(R, null, mAccelerometerSensorValues, mMagneticSensorValues);
            SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
            SensorManager.getOrientation(outR, values);

            convertValuesToDegress(values);

//            values[HEADING_INDEX] = values[HEADING_INDEX] + ADD_VALUE_HEADING;

            mSensorValues.setHeadingAngle(values[HEADING_INDEX]);

            mSensorValues.setPitchAngle(values[PITCH_INDEX]);
            mSensorValues.setRollAngle(values[ROLL_INDEX]);

            mListener.updateValuesSensor(mSensorValues);

        }
    }

    private void convertValuesToDegress(float[] values) {
        for (int i = 0; i < values.length; i++) {
            values[i] = (float) Math.toDegrees(values[i]);
        }
    }

    public void registerSensor() {
        if (!mSensorEnabled) {
            mSensorEnabled = true;
            mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
            mSensorManager.registerListener(accelerometerSensor, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) , SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(magneticSensor, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
        }

    }

    public void unregisterSensor() {
        if (mSensorEnabled) {
            mSensorEnabled = false;
            mSensorManager.unregisterListener(accelerometerSensor);
            mSensorManager.unregisterListener(magneticSensor);
        }
    }

    public boolean isSensorActivate() {
        return mSensorEnabled;
    }

}
