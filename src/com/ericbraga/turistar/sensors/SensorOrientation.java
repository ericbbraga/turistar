package com.ericbraga.turistar.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.ericbraga.turistar.observers.ListenerOrientationChange;
import com.ericbraga.turistar.pojo.SensorOrientationValues;

/**
 * Classe responsável por gerenciar os valores do Sensor
 * e repassar para as interfaces interessadas em modificações
 * nos valores desta sensor
 * @author ericbraga
 *
 */
@Deprecated
public class SensorOrientation implements SensorOrientationManager {

    private SensorManager mSensorManager;

    private Context mContext;

    private ListenerOrientationChange mListener;

    private SensorOrientationValues mSensorValues;

    private boolean mSensorEnabled = false;

    private final SensorEventListener eventListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
                updateSensorOrientationValues(event);
            }
        }

        /**
         * Apenas atualiza os valores do listener se o delta entre o
         * valor atual e o valor anterior do sensor foram maiores do que a
         * constante UPDATE_DELTA_VALUE. Isto foi feito para previnir
         * constantes alterações pequenas nos sensores.
         * 
         * @param event Valores atuais do sensor
         */
        private void updateSensorOrientationValues(SensorEvent event) {
            if (mSensorValues != null) {
                boolean necessaryUpdate = false;

                if (Math.abs(mSensorValues.getHeadingAngle() - event.values[HEADING_INDEX]) > UPDATE_DELTA_VALUE) {
                    mSensorValues.setHeadingAngle(event.values[HEADING_INDEX]);
                    necessaryUpdate = true;
                }

                if (Math.abs(mSensorValues.getPitchAngle() - event.values[PITCH_INDEX]) > UPDATE_DELTA_VALUE) {
                    mSensorValues.setPitchAngle(event.values[PITCH_INDEX]);
                    necessaryUpdate = true;
                }

                if (Math.abs(mSensorValues.getRollAngle() - event.values[ROLL_INDEX]) > UPDATE_DELTA_VALUE) {
                    mSensorValues.setRollAngle(event.values[ROLL_INDEX]);
                    necessaryUpdate = true;
                }

                if (necessaryUpdate) {
                    mListener.updateValuesSensor(mSensorValues);
                }
            }

        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) { /*Não precisei reimplementar*/ }
    };

    public SensorOrientation(Context pContext, ListenerOrientationChange pListener) {
        super();
        mSensorValues = new SensorOrientationValues();
        mContext      = pContext;
        mListener     = pListener;
    }

    public void registerSensor() {
        if (!isSensorActivate()) {
            mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
            Sensor orientationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
            /**
             * Registrei o listener para se atualizar com uma frequencia rápida para
             * poder capturar os dados mais rápido possível
             * */
            mSensorManager.registerListener(eventListener, orientationSensor, SensorManager.SENSOR_DELAY_UI);

            mSensorEnabled = true;
        }
    }

    public void unregisterSensor() {
        if (isSensorActivate()) {
            mSensorManager.unregisterListener(eventListener);
            mSensorEnabled = false;
        }
    }

    public boolean isSensorActivate() {
        return mSensorEnabled;
    }
}
