package com.ericbraga.turistar.sensors;

import com.ericbraga.turistar.observers.ListenerOrientationChange;

import android.content.Context;

/**
 * @author ericbraga
 * Classe responsável por retornar um dos dois
 * Sensores que poderão ser utilizados.
 */
public class SensorOrientationFactory {

    public static SensorOrientationManager getSensorOrientation (boolean isValuesPrecision, Context context, ListenerOrientationChange listener) {
        SensorOrientationManager sensorManager = null;

        if (isValuesPrecision) {
            sensorManager = new PrecisionSensorOrientation(context, listener);
        } else {
            sensorManager = new SensorOrientation(context, listener);
        }

        return sensorManager;
    }

}
