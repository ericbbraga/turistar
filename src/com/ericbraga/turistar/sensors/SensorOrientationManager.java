package com.ericbraga.turistar.sensors;

public interface SensorOrientationManager {

    /**
     * Índices que representam as posições no array de valores do sensor
     * de orientação. O sensor de orientação retorna estes
     * valores quando ocorre alguma alteração e um SensorEventListener
     * é registrado para este.
     * */
    public static final int HEADING_INDEX = 0;

    public static final int PITCH_INDEX = 1;

    public static final int ROLL_INDEX = 2;

    /**
     * Valor aceitável para poder atualizar os valores do objeto
     * sensorValues e assim evitar várias modificações no objeto
     * */
    public static final float UPDATE_DELTA_VALUE = 1.5f;

    public abstract void registerSensor();

    public abstract void unregisterSensor();

    public abstract boolean isSensorActivate();
}
