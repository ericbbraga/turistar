package com.ericbraga.turistar.test;

import android.os.Bundle;

import com.ericbraga.turistar.opengl.CameraView;

public class TuristARActivityCameraOpenGLTest extends TuristARActivityOpenGLTest {

    private CameraView cameraView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraView = new CameraView(getApplicationContext());
        addContentView(cameraView.getSurfaceView(), getLayoutParams());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
