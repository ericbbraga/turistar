package com.ericbraga.turistar.test;

import android.app.Activity;
import android.os.Bundle;

import com.ericbraga.turistar.observers.ListenerOrientationChange;
import com.ericbraga.turistar.pojo.SensorOrientationValues;
import com.ericbraga.turistar.sensors.SensorOrientationFactory;
import com.ericbraga.turistar.sensors.SensorOrientationManager;

public class TuristARActivityOrientation extends Activity {

    protected static final String TAG = "TuristARActivityOrientation";

    private SensorOrientationManager mSensorManager;

    private ListenerOrientationChange listener = new ListenerOrientationChange() {

        public void updateValuesSensor(SensorOrientationValues sensorValues) {
            android.util.Log.i(TAG, "sensor heading = " + sensorValues.getHeadingAngle());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSensorManager = SensorOrientationFactory.getSensorOrientation(true, getApplicationContext(), listener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerSensor();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterSensor();
    }

}
