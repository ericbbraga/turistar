package com.ericbraga.turistar.test;

import java.util.ArrayList;
import java.util.List;

import android.location.Location;

import com.ericbraga.turistar.pojo.TuristicLocationValues;

public class TuristicLocationValuesTest {
    private static List<TuristicLocationValues> turistLocationListTest;

    public static List<TuristicLocationValues> getTuristLocationListTest() {
        return turistLocationListTest;
    }

    public static Location createLocation(float latitude, float longitude) {
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        return location;
    }

    private static class MyLocation extends TuristicLocationValues {

        public MyLocation(float latitude, float longitude, String name) {
            super(createLocation(latitude, longitude), name);
        }
    }

    public static final TuristicLocationValues RUA_ITAPETIM = new MyLocation(-7.923769f,-34.823053f, "PONTO_0");

    public static final TuristicLocationValues RUA_CATENDE_JANGA = new MyLocation(-7.925131f,-34.82261f, "RUA_CATENDE_JANGA");
    
    public static final TuristicLocationValues RUA_SOLMAR = new MyLocation(-7.92028f,-34.829699f, "Rua Solmar");
    
    public static final TuristicLocationValues TORRE_MALAKOFF = new MyLocation(-8.060515f,-34.870714f, "Torre Malakoff");
    
    public static final TuristicLocationValues SINAGOGA_JUDEUS = new MyLocation(-8.06163f,-34.871347f, "Sinagoga dos Judeus");
    
    public static final TuristicLocationValues MARCO_ZERO = new MyLocation(-8.062395f,-34.871347f, "Marco Zero");

    public static final TuristicLocationValues RUA_FRANCISCO_SANTIAGO_TORRES_GALVAO = new MyLocation(-7.95453f,-34.87496f, "RUA_FRANCISCO_SANTIAGO_TORRES_GALVAO");

    public static final TuristicLocationValues RUA_DO_BRUM_RECIFE = new MyLocation(-8.056714f, -34.871613f, "RUA_DO_BRUM_RECIFE");

    static {
        turistLocationListTest = new ArrayList<TuristicLocationValues>();
        //turistLocationListTest.add(RUA_SOLMAR);
        turistLocationListTest.add(MARCO_ZERO);
        //turistLocationListTest.add(SINAGOGA_JUDEUS);
        //turistLocationListTest.add(TORRE_MALAKOFF);
        /*turistLocationListTest.add(RUA_CATENDE_JANGA);
        turistLocationListTest.add(RUA_DO_BRUM_RECIFE);
        turistLocationListTest.add(RUA_FRANCISCO_SANTIAGO_TORRES_GALVAO);*/
    }
}
